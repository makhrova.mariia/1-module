﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace module_2
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}


	

		private void buttonCount_Click_1(object sender, EventArgs e)
		{
			string sequenceText = textBox.Text;
			string[] numbers = sequenceText.Split(' ');

			int changeCount = 0;
			bool previousNumberIsEven = false;

			foreach (string number in numbers)
			{
				if (!string.IsNullOrEmpty(number) && double.TryParse(number, out double parsedNumber))
				{
					bool currentNumberIsEven = parsedNumber % 2 == 0;
					if (previousNumberIsEven != currentNumberIsEven)
					{
						changeCount++;
					}
					previousNumberIsEven = currentNumberIsEven;
				}
			}

			label2.Text = $"Кількість змін: {changeCount}";
		}
	}
}
